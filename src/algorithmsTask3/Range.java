package algorithmsTask3;

public class Range {

	private int start;
	private int stop;
	private String value;

	public Range(int start, int stop, String value) {
		this.start = start;
		this.stop = stop;
		this.value = value;
	}

	public boolean isInRange(int check) {
		if (check >= start && check <= stop) {
			return true;
		}
		return false;
	}

	public int getStart() {
		return start;
	}

	public void setStart(int start) {
		this.start = start;
	}

	public int getStop() {
		return stop;
	}

	public void setStop(int stop) {
		this.stop = stop;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
